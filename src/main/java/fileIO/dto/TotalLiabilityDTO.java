package fileIO.dto;

import lombok.Data;

@Data
public class TotalLiabilityDTO {

    private String currency;
    private int numBets;
    private float totalStakes;
    private float totalLiability;

}
