package fileIO.dto;

import lombok.Data;

@Data
public class SelectionLiabilityDTO {

    private String selectionName;
    private String currency;
    private int numBets;
    private float totalStakes;
    private float totalLiability;

}
