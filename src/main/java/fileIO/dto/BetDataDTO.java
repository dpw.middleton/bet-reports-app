package fileIO.dto;

import lombok.Data;

@Data
public class BetDataDTO {

    private String betId;
    private long timeStamp;
    private int selectionId;
    private String selectionName;
    private float stake;
    private float price;
    private String currency;

}
