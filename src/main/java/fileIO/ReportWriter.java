package fileIO;

import constants.OutputFormat;
import fileIO.dto.BetDataDTO;
import fileIO.dto.SelectionLiabilityDTO;
import fileIO.dto.TotalLiabilityDTO;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReportWriter {

    private OutputFormat outputFormat;
    private List<BetDataDTO> betData;

    private static final String SELECTION_LIABILITY_REPORT_NAME = "Selection Liability Report";
    private static final String SELECTION_LIABILITY_REPORT_FILE = "./Selection_Liability_Report.csv";
    private static final String SELECTION_LIABILITY_REPORT_HEADER = "Selection Name, Currency, Num Bets, Total Stakes, Total Liability \n";
    private static final String SELECTION_LIABILITY_DIVIDER = "----------------------------------------------------------------------------------------------------";
    private static final String TOTAL_LIABILITY_REPORT_NAME = "Total Liability Report";
    private static final String TOTAL_LIABILITY_REPORT_FILE = "./Total_Liability_Report.csv";
    private static final String TOTAL_LIABILITY_REPORT_HEADER = "Currency, No Of Bets, Total Stakes, Total Liability \n";
    private static final String TOTAL_LIABILITY_DIVIDER = "--------------------------------------------------------------------------------";

    public ReportWriter(OutputFormat outputFormat, List<BetDataDTO> betData) {
        this.outputFormat = outputFormat;
        this.betData = betData;
    }

    public void generateSelectionLiabilityByCurrencyReport() throws IOException {
        if (this.outputFormat == OutputFormat.CONSOLE) {
            outputSelectionLiabilityReportToConsole();
        } else if (this.outputFormat == OutputFormat.CSV) {
            outputSelectionLiabilityReportToCsv();
        }
    }

    private void outputSelectionLiabilityReportToCsv() throws IOException {
        List<SelectionLiabilityDTO> selectionLiabilities = new ArrayList<>();
        calculateAndSetSelectionLiabilitiesAndSort(selectionLiabilities);
        printSelectionLiabilityReportToCsv(selectionLiabilities);
    }

    private void outputSelectionLiabilityReportToConsole() {
        List<SelectionLiabilityDTO> selectionLiabilities = new ArrayList<>();
        calculateAndSetSelectionLiabilitiesAndSort(selectionLiabilities);
        printSelectionLiabilityReportToConsole(selectionLiabilities);
    }

    public void generateTotalLiabilityByCurrencyReport() throws IOException {
        if (this.outputFormat == OutputFormat.CONSOLE) {
            outputTotalLiabilityReportToConsole();
        } else if (this.outputFormat == OutputFormat.CSV) {
            outputTotalLiabilityReportToCsv();
        }
    }

    private void outputTotalLiabilityReportToCsv() throws IOException {
        List<TotalLiabilityDTO> totalLiabilities = new ArrayList<>();
        calculateAndSetTotalLiabilitiesAndSort(totalLiabilities);
        printTotalLiabilityReportToCsv(totalLiabilities);
    }

    private void outputTotalLiabilityReportToConsole() {
        List<TotalLiabilityDTO> totalLiabilities = new ArrayList<>();
        calculateAndSetTotalLiabilitiesAndSort(totalLiabilities);
        printTotalLiabilityReportToConsole(totalLiabilities);
    }

    private void calculateAndSetSelectionLiabilitiesAndSort(List<SelectionLiabilityDTO> selectionLiabilities) {
        for (BetDataDTO singleBet : this.betData) {
            SelectionLiabilityDTO selectionLiability = getSelectionLiabilityByNameAndCurrency(selectionLiabilities, singleBet.getSelectionName(), singleBet.getCurrency());
            if (null != selectionLiability) {
                selectionLiability.setNumBets(selectionLiability.getNumBets() + 1);
                selectionLiability.setTotalStakes(selectionLiability.getTotalStakes() + singleBet.getStake());
                selectionLiability.setTotalLiability(selectionLiability.getTotalLiability() + (singleBet.getStake() * singleBet.getPrice()));
            } else {
                selectionLiability = new SelectionLiabilityDTO();
                selectionLiability.setSelectionName(singleBet.getSelectionName());
                selectionLiability.setCurrency(singleBet.getCurrency());
                selectionLiability.setNumBets(1);
                selectionLiability.setTotalStakes(singleBet.getStake());
                selectionLiability.setTotalLiability(singleBet.getStake() * singleBet.getPrice());
                selectionLiabilities.add(selectionLiability);
            }
        }

        Collections.sort(selectionLiabilities, (o1, o2) -> o2.getCurrency().compareTo(o1.getCurrency()));
    }


    private void calculateAndSetTotalLiabilitiesAndSort(List<TotalLiabilityDTO> totalLiabilities) {
        for (BetDataDTO singleBet : this.betData) {
            TotalLiabilityDTO totalLiability = getTotalLiabilityByCurrency(totalLiabilities, singleBet.getCurrency());
            if (null != totalLiability) {
                totalLiability.setNumBets(totalLiability.getNumBets() + 1);
                totalLiability.setTotalStakes(totalLiability.getTotalStakes() + singleBet.getStake());
                totalLiability.setTotalLiability(totalLiability.getTotalLiability() + (singleBet.getStake() * singleBet.getPrice()));
            } else {
                totalLiability = new TotalLiabilityDTO();
                totalLiability.setCurrency(singleBet.getCurrency());
                totalLiability.setNumBets(1);
                totalLiability.setTotalStakes(singleBet.getStake());
                totalLiability.setTotalLiability(singleBet.getStake() * singleBet.getPrice());
                totalLiabilities.add(totalLiability);
            }
        }

        Collections.sort(totalLiabilities, (o1, o2) -> o2.getCurrency().compareTo(o1.getCurrency()));
    }


    private void printSelectionLiabilityReportToConsole(List<SelectionLiabilityDTO> selectionLiabilities) {
        System.out.format("%-40s %n", "\n" + SELECTION_LIABILITY_REPORT_NAME);
        System.out.format("%-20s %-20s %-20s %-20s %-20s %n", "Selection Name", "Currency", "Num Bets", "Total Stakes", "Total Liability");
        System.out.format("%-100s %n", SELECTION_LIABILITY_DIVIDER);
        for (SelectionLiabilityDTO selectionLiability : selectionLiabilities) {
            System.out.format("%-20s %-20s %-20d %-20.2f %-20.2f %n",
                    selectionLiability.getSelectionName(),
                    selectionLiability.getCurrency(),
                    selectionLiability.getNumBets(),
                    selectionLiability.getTotalStakes(),
                    selectionLiability.getTotalLiability());
        }
    }

    private void printTotalLiabilityReportToConsole(List<TotalLiabilityDTO> totalLiabilities) {
        System.out.format("%-40s %n", "\n" + TOTAL_LIABILITY_REPORT_NAME);
        System.out.format("%-20s %-20s %-20s %-20s %n", "Currency", "No Of Bets", "Total Stakes", "Total Liability");
        System.out.format("%-100s %n", TOTAL_LIABILITY_DIVIDER);
        for (TotalLiabilityDTO totalLiabilityDTO : totalLiabilities) {
            System.out.format("%-20s %-20d %-20.2f %-20.2f %n",
                    totalLiabilityDTO.getCurrency(),
                    totalLiabilityDTO.getNumBets(),
                    totalLiabilityDTO.getTotalStakes(),
                    totalLiabilityDTO.getTotalLiability());
        }
    }

    private void printSelectionLiabilityReportToCsv(List<SelectionLiabilityDTO> selectionLiabilities) throws IOException {
        StringBuffer fileContent = new StringBuffer(SELECTION_LIABILITY_REPORT_HEADER);
        for (SelectionLiabilityDTO selectionLiability : selectionLiabilities) {
            fileContent.append(selectionLiability.getSelectionName())
                    .append(", ")
                    .append(selectionLiability.getCurrency())
                    .append(", ")
                    .append(selectionLiability.getNumBets())
                    .append(", ")
                    .append(selectionLiability.getTotalStakes())
                    .append(", ")
                    .append(selectionLiability.getTotalLiability())
                    .append("\n");
        }
        printToCsv(fileContent, SELECTION_LIABILITY_REPORT_FILE);
    }

    private void printTotalLiabilityReportToCsv(List<TotalLiabilityDTO> totalLiabilities) throws IOException {
        StringBuffer fileContent = new StringBuffer(TOTAL_LIABILITY_REPORT_HEADER);
        for (TotalLiabilityDTO totalLiabilityDTO : totalLiabilities) {
            fileContent.append(totalLiabilityDTO.getCurrency())
                    .append(", ")
                    .append(totalLiabilityDTO.getNumBets())
                    .append(", ")
                    .append(totalLiabilityDTO.getTotalStakes())
                    .append(", ")
                    .append(totalLiabilityDTO.getTotalLiability())
                    .append("\n");
        }
        printToCsv(fileContent, TOTAL_LIABILITY_REPORT_FILE);

    }

    private void printToCsv(StringBuffer fileContent, String reportFile) throws IOException {
        removeFileIfItAlreadyExists(reportFile);
        try (RandomAccessFile stream = new RandomAccessFile(reportFile, "rw");
                FileChannel channel = stream.getChannel()) {
            byte[] strBytes = fileContent.toString().getBytes();
            ByteBuffer buffer = ByteBuffer.allocate(strBytes.length);
            buffer.put(strBytes);
            buffer.flip();
            channel.write(buffer);
        }
    }

    private void removeFileIfItAlreadyExists(String reportFile) {
        File file = new File(reportFile);
        if (file.exists()) {
            file.delete();
        }
    }

    private SelectionLiabilityDTO getSelectionLiabilityByNameAndCurrency(List<SelectionLiabilityDTO> selectionLiabilities, String selectionName, String currency) {
        return selectionLiabilities
                .stream()
                .filter(item -> selectionName.equals(item.getSelectionName()))
                .filter(item -> currency.equals(item.getCurrency()))
                .findFirst()
                .orElse(null);
    }


    private TotalLiabilityDTO getTotalLiabilityByCurrency(List<TotalLiabilityDTO> totalLiabilities, String currency) {
        return totalLiabilities
                .stream()
                .filter(item -> currency.equals(item.getCurrency()))
                .findFirst()
                .orElse(null);
    }
}
