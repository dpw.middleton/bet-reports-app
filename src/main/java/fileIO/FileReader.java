package fileIO;

import constants.InputFormat;
import fileIO.dto.BetDataDTO;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpHeaders;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class FileReader {

    private String header = "betId, betTimestamp, selectionId, selectionName, stake, price, currency";
    private InputFormat inputFormat;
    private Response response;
    private RequestSpecification requestSpec;
    URL url;

    public FileReader(InputFormat inputFormat) {
        this.inputFormat = inputFormat;
    }


    public List<BetDataDTO> getBetDataFromFileSource(String fileSource) throws FileNotFoundException, MalformedURLException {
        if (this.inputFormat == InputFormat.CSV) {
            return readDataFromCSV(fileSource);
        } else if (this.inputFormat == InputFormat.JSON) {
            return readDataFromJSON(fileSource);
        } else return new ArrayList<>();
    }

    private List<BetDataDTO> readDataFromJSON(String fileSource) throws MalformedURLException {
        List<BetDataDTO> betData = new ArrayList<>();
        url = new URL(fileSource);
        baseURI = fileSource;
        requestSpec = new RequestSpecBuilder().
                addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .build();

        response = given()
                .spec(requestSpec)
                .when()
                .log()
                .all()
                .get(baseURI);

        JsonPath jsonPath = response.getBody().jsonPath();
        List<Map<String, Object>> itemsList = jsonPath.getList("items");
        for (Map<String, Object> item : itemsList) {
            betData.add(getDataFromItem(item));
        }

        return betData;
    }

    private BetDataDTO getDataFromItem(Map<String, Object> item) {
        BetDataDTO betData = new BetDataDTO();

        betData.setBetId((String) item.get("betId"));
        betData.setTimeStamp((Long) item.get("timestamp"));
        betData.setSelectionId((Integer) item.get("selectionId"));
        betData.setSelectionName((String) item.get("selectionName"));
        betData.setStake(Float.parseFloat((String) item.get("stake")));
        betData.setPrice(Float.parseFloat((String) item.get("price")));
        betData.setCurrency((String) item.get("currency"));

        return betData;
    }

    private List<BetDataDTO> readDataFromCSV(String fileSource) throws FileNotFoundException {
        List<BetDataDTO> betData = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileSource))) {
            scanner.useDelimiter("\n");

            while (scanner.hasNext()) {
                String line = scanner.next().trim();
                if (!header.equals(line)) {
                    BetDataDTO betDataDTO = getDataFromBetLine(line);
                    betData.add(betDataDTO);
                }
            }
        }

        return betData;
    }

    private BetDataDTO getDataFromBetLine(String line) {
        BetDataDTO betData = new BetDataDTO();
        String[] items = line.split(",");

        betData.setBetId(items[0].trim());
        betData.setTimeStamp(Long.parseLong(items[1].trim()));
        betData.setSelectionId(Integer.parseInt(items[2].trim()));
        betData.setSelectionName(items[3].trim());
        betData.setStake(Float.parseFloat(items[4].trim()));
        betData.setPrice(Float.parseFloat(items[5].trim()));
        betData.setCurrency(items[6].trim());

        return betData;
    }
}
