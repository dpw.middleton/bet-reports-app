package validation;

import constants.InputFormat;
import constants.OutputFormat;

import java.io.File;

public class Validator {

    public void validateArguments(String fileSource, String dataFormat, String outputFormat) {
        StringBuilder invalidMessages = new StringBuilder("");

        invalidMessages.append(validateFileSource(fileSource, dataFormat));
        invalidMessages.append(validateFileFormat(dataFormat));
        invalidMessages.append(validateOutputFormat(outputFormat));

        if (!invalidMessages.toString().isEmpty()) {
            throw new IllegalArgumentException("\n" + invalidMessages.toString());
        }
    }

    private String validateOutputFormat(String outputFormat) {
        try {
            OutputFormat.valueOf(outputFormat.toUpperCase());
        } catch (IllegalArgumentException e) {
            return "\n" + e.getMessage();
        }

        return "";
    }

    private String validateFileFormat(String fileFormat) {
        try {
            InputFormat.valueOf(fileFormat.toUpperCase());
        } catch (IllegalArgumentException e) {
            return "\n" + e.getMessage();
        }

        return "";
    }

    private String validateFileSource(String fileSource, String dataFormat) {
        if (dataFormat.equals(InputFormat.CSV.toString())) {
            File file = new File(fileSource);

            if (!file.exists()) {
                return "Input file does not exist: " + fileSource;
            } else if (file.isDirectory()) {
                return "Input file is a directory: " + fileSource;
            }
        } else if (dataFormat.equals(InputFormat.JSON.toString())) {
            return "";
        }


        return "";
    }
}
