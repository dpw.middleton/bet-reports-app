package reports;

import constants.InputFormat;
import constants.OutputFormat;
import fileIO.FileReader;
import fileIO.ReportWriter;
import fileIO.dto.BetDataDTO;
import validation.Validator;

import java.io.IOException;
import java.util.List;

public class ReportsGenerator {

    private static Validator validator = new Validator();

    public static void main(String args[]) throws IOException {
        String fileSource = args[0];
        String dataFormat = args[1];
        String outputFormat = args[2];

        System.out.println("File Source: " + fileSource);
        System.out.println("Input Data Format: " + dataFormat);
        System.out.println("Output Report Format: " + outputFormat);

        validator.validateArguments(fileSource, dataFormat, outputFormat);

        FileReader fileReader = new FileReader(InputFormat.valueOf(dataFormat.toUpperCase()));
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(fileSource);
        System.out.println(betData.get(1));

        ReportWriter reportWriter = new ReportWriter(OutputFormat.valueOf(outputFormat.toUpperCase()), betData);
        reportWriter.generateSelectionLiabilityByCurrencyReport();
        reportWriter.generateTotalLiabilityByCurrencyReport();
    }

}
