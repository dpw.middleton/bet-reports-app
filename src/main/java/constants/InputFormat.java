package constants;

public enum InputFormat {
    CSV, JSON;
}
