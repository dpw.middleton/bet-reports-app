package constants;

public enum OutputFormat {
    CONSOLE, CSV;
}
