package fileIO;

import constants.InputFormat;
import fileIO.dto.BetDataDTO;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class FileReaderTests {

    private static final String validTestCSVInput = "src/test/resources/valid.csv";
    private static final String invalidTimeStampCSVInput = "src/test/resources/invalidTimeStamp.csv";
    private static final String invalidSelectionIdCSVInput = "src/test/resources/invalidSelectionId.csv";
    private static final String invalidStakeCSVInput = "src/test/resources/invalidStake.csv";
    private static final String invalidPriceCSVInput = "src/test/resources/invalidPrice.csv";

    private static final String validTestJSONInput = "https://bet-data-bd9ae-default-rtdb.europe-west1.firebasedatabase.app/valid.json";
    private static final String invalidTimeStampJSONInput = "https://bet-data-bd9ae-default-rtdb.europe-west1.firebasedatabase.app/invalid-timestamp.json";
    private static final String invalidSelectionIdJSONInput = "https://bet-data-bd9ae-default-rtdb.europe-west1.firebasedatabase.app/invalid-selection-id.json";
    private static final String invalidStakeJSONInput = "https://bet-data-bd9ae-default-rtdb.europe-west1.firebasedatabase.app/invalid-stake.json";
    private static final String invalidPriceJSONInput = "https://bet-data-bd9ae-default-rtdb.europe-west1.firebasedatabase.app/invalid-price.json";

    private FileReader fileReader;


    @Test
    public void testGetBetDataFromFileSource_WithCorrectDataInCSV() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.CSV);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(validTestCSVInput);
        assertTrue(betData.get(0).getBetId().equals("Bet-10"));
        assertTrue(new Long(betData.get(0).getTimeStamp()).equals(new Long(1489490156000L)));
        assertTrue(new Integer(betData.get(0).getSelectionId()).equals(1));
        assertTrue(betData.get(0).getSelectionName().equals("My Fair Lady"));
        assertTrue(new Float(betData.get(0).getStake()).equals(new Float(0.5)));
        assertTrue(new Float(betData.get(0).getPrice()).equals(new Float(6.0)));
        assertTrue(betData.get(0).getCurrency().equals("GBP"));
    }

    @Test(expected = NumberFormatException.class)
    public void testGetBetDataFromFileSource_WithInvalidTimeStampDataInCSV() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.CSV);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidTimeStampCSVInput);
    }

    @Test(expected = NumberFormatException.class)
    public void testGetBetDataFromFileSource_WithInvalidSelectionIdDataInCSV() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.CSV);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidSelectionIdCSVInput);
    }

    @Test(expected = NumberFormatException.class)
    public void testGetBetDataFromFileSource_WithInvalidStakeDataInCSV() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.CSV);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidStakeCSVInput);
    }

    @Test(expected = NumberFormatException.class)
    public void testGetBetDataFromFileSource_WithInvalidPriceDataInCSV() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.CSV);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidPriceCSVInput);
    }

    @Test
    public void testGetBetDataFromFileSource_WithCorrectDataInJSON() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.JSON);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(validTestJSONInput);
        assertTrue(betData.get(0).getBetId().equals("Bet-10"));
        assertTrue(new Long(betData.get(0).getTimeStamp()).equals(new Long(1489490156000L)));
        assertTrue(new Integer(betData.get(0).getSelectionId()).equals(1));
        assertTrue(betData.get(0).getSelectionName().equals("My Fair Lady"));
        assertTrue(new Float(betData.get(0).getStake()).equals(new Float(0.5)));
        assertTrue(new Float(betData.get(0).getPrice()).equals(new Float(6.0)));
        assertTrue(betData.get(0).getCurrency().equals("GBP"));
    }

    @Test(expected = ClassCastException.class)
    public void testGetBetDataFromFileSource_WithInvalidTimeStampDataInJSON() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.JSON);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidTimeStampJSONInput);
    }

    @Test(expected = ClassCastException.class)
    public void testGetBetDataFromFileSource_WithInvalidSelectionIdDataInJSON() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.JSON);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidSelectionIdJSONInput);
    }

    @Test(expected = ClassCastException.class)
    public void testGetBetDataFromFileSource_WithInvalidStakeDataInJSON() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.JSON);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidStakeJSONInput);
    }

    @Test(expected = ClassCastException.class)
    public void testGetBetDataFromFileSource_WithInvalidPriceDataInJSON() throws FileNotFoundException, MalformedURLException {
        fileReader = new FileReader(InputFormat.JSON);
        List<BetDataDTO> betData = fileReader.getBetDataFromFileSource(invalidPriceJSONInput);
    }
}
