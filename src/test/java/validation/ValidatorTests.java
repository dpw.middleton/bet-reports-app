package validation;

import constants.InputFormat;
import constants.OutputFormat;
import org.junit.Before;
import org.junit.Test;


public class ValidatorTests {

    private static String validTestInput = "src/test/resources/valid.csv";
    private static String nonExistingFile = "src/test/resources/nonExistingTestFile.csv";

    private Validator validator;

    @Before
    public void setUp() {
        validator = new Validator();
    }

    @Test
    public void testValidateArguments_usingCorrectArguments() {
        validator.validateArguments(validTestInput, InputFormat.CSV.toString(), OutputFormat.CSV.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateArguments_exceptionThrownWhenUsingNonExistingCSVFile() {
        validator.validateArguments(nonExistingFile, InputFormat.CSV.toString(), OutputFormat.CSV.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateArguments_exceptionThrownWhenUsingInvalidInputFormat() {
        validator.validateArguments(validTestInput, "txt", OutputFormat.CSV.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateArguments_exceptionThrownWhenUsingInvalidOutputFormat() {
        validator.validateArguments(validTestInput, InputFormat.CSV.toString(), "txt");
    }
}
