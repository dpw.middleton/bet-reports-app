# Bet Reports App

This application produces the following two reports using a provided bet data file 

- Selection Liability By Currency
- Total Liability By Currency

It gives the user the following options:

- Choose the source and format of the data 
    - Local CSV file
    - JSON data over HTTP
  

- Choose the output format 
    - text printed to the console 
    - CSV file

#Compilation

Compiled using mvn clean install.

#Running the Programme

To run the application you must include three arguments in this order:

- The data source
- The input data format
- The report output format

#Command Line Options

The following options for the command line arguments are allowed:

- Input Data File Format Options
  - CSV
  - JSON

- File Source:
  - For CSV, this should be the absolute path and name of the input data file
  - For JSON, this should be the URL for the endpoint where the data is kept

- Report Output Format Options
  - Console
  - CSV

#Expected Input Data Structure

- CSV

    betId, betTimestamp, selectionId, selectionName, stake, price, currency

    Bet-10, 1489490156000, 1, My Fair Lady, 0.5, 6.0, GBP

    Bet-11, 1489490156000, 2, Always a Runner, 1.25, 4.0, EUR

- JSON

See https://bet-data-bd9ae-default-rtdb.europe-west1.firebasedatabase.app/valid.json
for an example REST endpoint that returns valid JSON data.

  ```json 
  {
    "items" : [
      {
        "betId" : "Bet-10",
        "currency" : "GBP",
        "price" : "6.0",
        "selectionId" : 1,
        "selectionName" : "My Fair Lady",
        "stake" : "0.5",
        "timestamp" : 1489490156000
      },
      {
        "betId" : "Bet-11",
        "currency" : "EUR",
        "price" : "4.0",
        "selectionId" : 2,
        "selectionName" : "Always a Runner",
        "stake" : "1.25",
        "timestamp" : 1489490156000
      }
    ]
  }
```
